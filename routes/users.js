const router = require('express').Router();
const User = require('../models/Users.js');
const bcrypt = require('bcrypt');
const passport = require('passport');
require('./../passport-setup.js');
const jwt = require('jsonwebtoken');

//create for register
router.post('/register', (req, res, next) => {
	// res.send("This is register endpoint.") <-- this is to check lang;

	//1st get the details from the form
	//2nd check the completeness of the form
	//check if password is not less than 8 chars
	//check if  password and confirm password matched


	//1st get the details from the form
	let firstname = req.body.firstname;
	let lastname = req.body.lastname;
	let email = req.body.email;
	let password = req.body.password;
	let confirmPassword = req.body.confirmPassword;

	//2nd check the completeness of the form
	if(!firstname || !lastname || !email || !password || !confirmPassword) {
		return res.status(400).send({
			message: "Incomplete fields ito"
		})
	};

	//check if password is not less than 8 chars
	if(password.length < 8) {
		return res.status(400).send({
			message: "Ang password mo ay masyadong maikli."
		})
	};

	//check if  password and confirm password matched
	if(password !== confirmPassword) {
		return res.status(400).send({
			message: "Ang password mo at confirmPassword ay di parehas."
		})
	};

	//check if email is already in use
	User.findOne( {
		email: email
	}).then( (user) => {
		if(user) {
			return res.status(400).send({
				message: "Email is already taken."
			})
		} else {

			const saltRounds = 10;
			bcrypt.genSalt(saltRounds, function(err, salt) {
				bcrypt.hash(password, salt, function(err, hash) {

					User.create({
						firstname,
						lastname,
						email,
						password:hash
					}).then( (user) => {
						return res.send(user);
					})
				})
			})

			
		}
	})


})




router.post('/profile', passport.authenticate('jwt', { session: false }),
    function(req, res) {
        res.send(req.user);
    }
);

//create for login

router.post('/login', (req, res, next) => {
	let email = req.body.email;
	let password = req.body.password;

	//check if there are credentials
	if(!email || !password) {
		return res.status(400).send({
			message: "Something went wrong."
		})
	}

	//check if it is registered or matched
	User.findOne({
		email
	}).then( (user) => {
		//if there are no email match
		if(!user) {
			return  res.status(400).send({
				message: "Something went wrongZ"
			})
		} else {
			//check if matched password with email holder
			bcrypt.compare(password, user.password, (err, passwordMatch) => {
				if (passwordMatch) {

					let token = jwt.sign({id: user._id}, 'secret');

					return res.send({
						message: "Login successful.",
						token: token
					})
				} else {
					return res.send({
						message: "Something went wrong."
					})
				}
			})
		}
	})
})

module.exports = router;