const express = require('express'); //pwede rin require('express').Router();
const router = express.Router();

const AirVehicleModel = require('../models/AirVehicles');
const multer = require('multer');

const passport = require('passport');
const checker = require('./../checker.js');
const passportAuth = passport.authenticate('jwt', { session: false });

const storage = multer.diskStorage({
	destination : function(req, file, cb) {
		cb(null, "public/airvehicles")
	},
	filename : function(req, file, cb) {
		cb(null, Date.now() + '-' + file.originalname)
	}
})
const upload = multer({ storage : storage});


// yung dati ito --> const upload = multer({ dest : 'uploads/'});


//index
router.get('/', function(req,res, next){
	AirVehicleModel.find().then(
		airvehicles => {
			res.json(airvehicles)
		}).catch(next)
});

//single  other method: findById ------> findById(req.params.id)
router.get('/:id', (req,res, next) => {
	AirVehicleModel.findOne({
		_id : req.params.id
	}).then(
			product => res.json(product)).catch(next);
});


//create note: idagdag ang upload.single('name attribute in form') para sa upload photo
router.post('/', passportAuth, checker , upload.single('image') , (req,res, next) => {
	// res.json(req.file)

	checker.roleChecker;

	req.body.image = "/public/" + req.file.filename;
	AirVehicleModel.create( req.body
		
	).then(
			product => res.send(product)).catch(next);


})

//put other method: findByIdAndUpdate -------> (req.params.id, req.body)
router.put('/:id',
	passportAuth,
	checker,
	upload.single('image'),
	(req,res, next) => {
	/*AirVehicleModel.findOneAndUpdate({
		_id : req.params.id
	},
	{
		name: req.body.name,
		categoryId: req.body.categoryId,
		price: req.body.price,
		description: req.body.description,
		image: req.body.image
	},

	{
		new : true

	})*/

	// checker.roleChecker;

	let update = {
		...req.body
	}

	if(req.file) {
		update = {
			...req.body,
			image: "/public/" + req.file.filename
		}
	}

	// req.body.image = "/public/" + req.file.filename;


	AirVehicleModel.findByIdAndUpdate( req.params.id, update, {new:true})

	.then(product => res.json(product))
		.catch(next)
})

//delete other method: deleteOne
router.delete('/:id',passportAuth, checker , (req,res, next) => {
	AirVehicleModel.findOneAndDelete({
		_id : req.params.id
	}).then(product => res.json(product))
		.catch(next)
})

module.exports =  router;
