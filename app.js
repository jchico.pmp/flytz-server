const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const passport = require('passport');

//set up server

//initialize the app

const app = express();

//localhost port
// const port = 3001;

// online
const port = process.env.PORT || 8000;

//connect to database
/*mongoose.connect(
	'mongodb://localhost:27017/flytz',
	 {
	 	useNewUrlParser: true,
	 	useUnifiedTopology: true,
	 	
	 });
mongoose.connect('connected', () => {
	console.log("NAKA-CONNECT KA NA SA DATABASE!!");
})
*/

/* localhost
mongoose.connect('mongodb://localhost/flytz', () => {
	console.log('Connected to database.')
})

*/

//connect to online database

mongoose.connect('mongodb+srv://admin:test1234@capstone-3-zq4no.mongodb.net/test?retryWrites=true&w=majority', () => {
	console.log("NAKACONNECT KA NA SA DATABASE!!!")
});


//use dependencies/middleware
app.use(bodyParser.json());
app.use(cors());
// app.use('/public', express.static('public/products'));
app.use((req,res,next) => {
	console.log(req.body)
	next()
})
app.use(passport.initialize());



// Routes

app.use('/categories', require('./routes/categories'));
app.use('/airvehicles',require('./routes/airvehicles'));
app.use('/users', require('./routes/users.js'));
// app.use('/transactions', require('./routes/transactions.js'));


//error handling middleware
app.use(function(err,req,res,next) {
		res.status(422).json({
				error : err.message,
		});
		// console.log(err);
})


app.listen(port, () => {
	console.log(`Listening to port ${port}`);
})

